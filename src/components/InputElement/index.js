import React from 'react';

function InputElement(props){
    return(
        <div>
            <input value={ props.value} onChange={ props.onChange} />
            <button onClick={props.onAddElement}>Add to array</button>
        </div>
    )
}


export default InputElement;