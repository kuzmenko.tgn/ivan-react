import React from 'react';
import ListElement from './components/ListElement';
import InputElement from './components/InputElement';

class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      inputOn : '',
      prevInputOn: '',
      dataName : []
    };
  }
  



  onInputChange = (event) => {
    this.setState({
      inputOn : event.target.value
    })
  }

  onButtonClick = () => {
    if(this.state.inputOn === '') {return}
    this.setState({
        inputOn : '',
        prevInputOn : this.state.inputOn,
        dataName : this.state.dataName.concat([this.state.inputOn])
     }
    )
  }

  deleteElement = (index) => {
    const start = this.state.dataName.slice(0, index);
    const end = this.state.dataName.slice(index + 1);
    const newDataName = start.concat(end)
    this.setState({
      dataName : newDataName
    })
    
  }

  

  render() {
    const helloMessage = 'Hello ' + this.state.inputOn;
    console.log(this.state.dataName)
    return (
      <div>
        
        <h1>{helloMessage}</h1>
        <InputElement value = { this.state.inputOn} onChange={this.onInputChange} onAddElement= {this.onButtonClick}/>
        <p>prev {this.state.prevInputOn}</p>
        {this.state.dataName.map(
          (listName, index) => <ListElement hui={() => this.deleteElement(index)} key = {index} name = {listName}/>
        )}
      </div>
    )
  }
}


export default App;
